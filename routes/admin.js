const express = require('express')
const router = express.Router();
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();


const authHandler = require('../authHandler/auth.js');
const authCtrl = require('../controllers/admin/authCtrl.js');
const staticCtrl = require('../controllers/admin/staticCtrl.js')
const dashboardCtrl = require('../controllers/admin/dashboardCtrl.js')
const subscriptionCtrl = require('../controllers/admin/subscriptionCtrl.js')
const notificationCtrl = require('../controllers/admin/notificationCtrl')



router.post('/login', authCtrl.adminLogin);
router.post('/sendOtpOnEmail', authCtrl.sendOtpOnEmail);
router.post('/verifyOtp', authCtrl.verifyOtp);
router.post('/forgetPassword', authCtrl.forgetPassword);
router.post('/createPin', authCtrl.createPin);
router.post('/verifyPinPassword', authCtrl.verifyPinPassword);



//DASHBOARD
router.post('/dashboard', dashboardCtrl.dashboard);
// router.post('/adminNotificationList', dashboardCtrl.adminNotificationList);



// STATIC  MANAGEMENT

router.post('/addPrivacyAndPolicy', staticCtrl.addPrivacyAndPolicy);
router.post('/getPrivacyAndPolicy', staticCtrl.getPrivacyAndPolicy);
router.post('/updatePrivacyAndPolicy', staticCtrl.updatePrivacyAndPolicy);


router.post('/addAboutUs', staticCtrl.addAboutUs);
router.post('/getAboutUs', staticCtrl.getAboutUs);
router.post('/updateAboutUs', staticCtrl.updateAboutUs);


router.post('/addTermsAndCond', staticCtrl.addTermsAndCond);
router.post('/getTermsAndCond', staticCtrl.getTermsAndCond);
router.post('/updateTermsAndCond', staticCtrl.updateTermsAndCond);



// SUBSCRIPTION  

router.post('/addSubscription', subscriptionCtrl.addsubscription);
router.post('/subscriptionList', subscriptionCtrl.subscriptionList);


// NOTIFICATION

router.post('/addNotifications', notificationCtrl.addNotifications);
router.post('/notificationsList', notificationCtrl.notificationsList);
router.post('/resendNotifications', notificationCtrl.resendNotifications);






router.use(authHandler.authAdmin);

router.get('/adminDetails', authCtrl.adminDetails);
router.post('/changePassword', authCtrl.changePassword);
router.put('/updateAdmin', authCtrl.updateAdmin);
router.post('/adminLogout', authCtrl.adminLogout);



module.exports = router;