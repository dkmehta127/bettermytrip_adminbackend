const mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate');
let mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
let Schema = mongoose.Schema;
var func = require('../utility/commonFun.js');
const Admin = mongoose.Schema({

    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    username: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    plainPassword: {
        type: String
    },
    hotel_number: {
        type: Number,
        default: 100
    },
   
    profilePic: {
        type: String,
        default: "https://res.cloudinary.com/a2karya80559188/image/upload/v1584446275/admin_nke1cg.jpg"
    },
    age: {
        type: Number
    },
    dob: {
        type: String
    },
    idCard: {
        type: String
    },
    address: {
        type: String
    },
    gender: {
        type: String,
        enum: ['Male', 'Female', 'Others'],
        default: 'Male'
    },
    countryCode: {
        type: String
    },
    phone: {
        type: String
    },
    addedBy: {
        type: String
    },
    addedById: {
        type: Schema.Types.ObjectId,
        ref: "admins"
    },
    updatedBy: {
        type: String
    },
    updatedById: {
        type: Schema.Types.ObjectId,
        ref: "admins"
    },
    userType: {
        type: String,
        enum: ['Admin', 'SubAdmin', 'Reseller'],
        default: 'Admin'
    },
    permission: {
        type: Array,
        default: ['All']
    },
    forgotPassMilisec: {
        type: String,
    },
    forgotPassToken: {
        type: String,
        trim: true
    },
    jwtToken: {
        type: String
    },
    otp: {
        type: String
    },
    pin: {
        type: String
    },

    officialWebsite:{
        type:String
    },

    is_delete:{
        type : Boolean,
        default: false
    },


    status: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active'
    }

}, {
    timestamps: true
})

Admin.plugin(mongoosePaginate);
Admin.plugin(mongooseAggregatePaginate);


const AdminModel = mongoose.model('admins', Admin, 'admins');
module.exports = AdminModel
AdminModel.findOne({}, (error, success) => {
    if (error) {
        console.log(error)
    } else {
        if (!success) {
            func.bcrypt("admin123", (err, password) => {
                if (err)
                    console.log("Error is=============>", err)
                else {
                    new AdminModel({
                        email: "battermytrip@yopmail.com",
                        password: password,
                        plainPassword: "admin123",
                        username: "Admin",
                        firstName: "Better-my-trip-admin",
                        lastName: "admin",
                        countryCode: "+91",
                        phone: "9720546333",
                        officialWebsite:"https://www.b2be-commerce.com/",
                        userType: "Admin",
                        address:"testing location ",
                        profilePic: "https://res.cloudinary.com/a2karya80559188/image/upload/v1584446275/admin_nke1cg.jpg"
                    }).save((error, success) => {
                        if (error) {
                            console.log("Error in creating admin");
                        }
                        else {
                            console.log("Admin created successfully");
                            console.log("Admin data is==========>", success);
                        }
                    })
                }
            })
        }
    }
})
