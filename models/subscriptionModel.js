const mongoose = require("mongoose");

const modelSchema = new mongoose.Schema({

    title: {
        type: String
    },
    duration: {
        type: String
    },
    price: {
        type: String
    },

    description: {
        type: String
    },


    is_active: {
        type: Boolean,
        default: true,
    }
}, {
    timestamps: true
})

const models = new mongoose.model('subscriptions', modelSchema);

module.exports = models;