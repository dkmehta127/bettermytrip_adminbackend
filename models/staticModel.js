const mongoose = require("mongoose");

const modelSchema = new mongoose.Schema({

    title: {
        type: String
    },
    data: {
        type: String
    },
    is_for: {
        type: String
    },

    bankList: {
        type: Array
    },

    contactNo: {
        type: String
    },
    email: {
        type: String
    },
    address: {
        type: String
    },

    is_active: {
        type: Boolean,
        default: true,
    }
}, {
    timestamps: true
})

const models = new mongoose.model('static_contents', modelSchema);

module.exports = models;