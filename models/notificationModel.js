const mongoose = require("mongoose");

let Schema = mongoose.Schema;
const modelSchema = new mongoose.Schema({

    user: {
        type: Schema.Types.ObjectId
    },
    title: {
        type: String
    },
    content: {
        type: String
    },
    status: {
        type: String,
        default: 'unread',
    },
    data:{
        type:String
    },
    for:{
        type:String
    },
    is_deleted: {
        type: Boolean,
        default: false,
    }
}, {
    timestamps: true
})


const models = new mongoose.model('notifications', modelSchema);

module.exports = models;