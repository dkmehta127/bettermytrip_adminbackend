const express   = require("express");
const mongoose  = require("mongoose")
const app       = express();
const https = require('https');
const fs = require('fs');

var adminRouter = require('./routes/admin');
var indexRouter = require('./routes/index');

const cors       = require('cors');
const bodyParser = require('body-parser')
const morgan     = require('morgan');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(morgan('combined'))
app.set('trust proxy', true);

//mongoose.set('useNewUrlParser', true);
// mongoose.set('useFindAndModify', false);
// mongoose.set('useCreateIndex', true);
// mongoose.set('useUnifiedTopology', true);

app.use(cors());

mongoose.connect(`mongodb://127.0.0.1:27017/bettermytrip`, { useNewUrlParser: true}, (err, result) => {
  if (err) {
    console.log("Error in connecting with database")
  }
  else {
    console.log('Mongoose connecting is setup successfully')
  }
});

// const httpsOptions = {
//   cert : fs.readFileSync('/var/www/ssl_files/inorderapp_com_au.crt'),
//   ca : fs.readFileSync('/var/www/ssl_files/inorderapp_com_au.ca-bundle'),
//   key : fs.readFileSync('/var/www/ssl_files/inorderapp.com_au.key'),
// }

// var server = https.createServer(httpsOptions, app);
// server.listen(4000, () => {
//   console.log(`Server running on ssl port 4000`);
// })


//==========================Request Console=======================//

app.all("*", (req, resp, next) => {
  
  let obj = {
    Host: req.headers.host,
    ContentType: req.headers['content-type'],
    Url: req.originalUrl,
    Method: req.method,
    Query: req.query,
    Body: req.body,
    Parmas: req.params[0]
  }
  console.log("Common Request is===========>", [obj])
  next();
});

app.use('',indexRouter);
app.use('/api/v1/admin', adminRouter);




const port = 7021;
app.listen(port, () => {
    console.log(`You pick up listening on port ${port}`);
})

