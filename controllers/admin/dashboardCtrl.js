const {
    ObjectId
} = require('mongodb');


const response = require("../../utility/httpResponseMessage");
const statusCode = require("../../utility/httpResponseCode");
const ADMIN = require('../../models/adminModel')
// const INSTITUTE = require('../../models/instituteModel')




const {
    query,
    body,
    check,
    oneOf,
    validationResult
} = require('express-validator');
const errorFormatter = ({
    location,
    msg,
    param,
    value,
    nestedErrors
}) => {
    return `${location}[${param}]: ${msg}`;
};
const request = require('request');

const moment = require('moment');
const date = require('date-and-time');
var ISODate = require("isodate");

module.exports = {

    dashboard: async (req, res) => {
        try {
            let search = {
                is_delete: false
            }
            if (!req.body.startDate == '' && !req.body.endDate == "") {
                let endDate = new Date(req.body.endDate)
                endDate.setDate(endDate.getDate() + 1)
                console.log("endDates==>", endDate)
                search.createdAt = {
                    $gte: new Date(req.body.startDate),
                    $lte: endDate
                }
            }
            if (req.body.timeFrame == "Today") {
                // let startTodayDate = new Date(moment().format())
                let setTodayDate = new Date()
                setTodayDate.setHours(0, 0, 59, 999);
                let todayDate = new Date()
                todayDate.setHours(23, 59, 59, 999);
                search.createdAt = {
                    $gte: setTodayDate,
                    $lte: todayDate
                }
            }
            if (req.body.timeFrame == "Week") {
                let weekDate = new Date(moment().startOf('isoWeek').format())
                let todayDate = new Date()
                todayDate.setHours(23, 59, 59, 999);
                search.createdAt = {
                    $gte: weekDate,
                    $lte: todayDate
                }
            }
            if (req.body.timeFrame == "Month") {
                let monthDate = new Date(moment().clone().startOf('month').format())
                let todayDate = new Date()
                todayDate.setHours(23, 59, 59, 999);
                search.createdAt = {
                    $gte: monthDate,
                    $lte: todayDate
                }
            }

            // let totalColleges = await COLLEGES.find({
            // ...search,
            //     adminVerificationStatus : 'Approved'
            // }).countDocuments();

            let  totalHotel = 0
            let  totalGuests = 0
            let  totalJobApplicants = 0
           

            if (totalHotel || totalGuests ) {
                let data = {
                    totalHotel: 0,
                    totalGuests:  0,
                    totalColleges: 0,
                    totalRevenue: 0,
                    totalWalletBalance: 0,

                }
                return response.responseHandlerWithData(res, 200, 'Data found', data);
            }else{
                let data = {
                    totalHotel: 0,
                    totalGuests:  0,
                    totalColleges: 0,
                    totalRevenue: 0,
                    totalWalletBalance: 0,

                }
                return response.responseHandlerWithData(res, 200, 'Data found', data);
            }
        } catch (error) {
            console.log(error)
            return response.responseHandlerWithMessage(res, 500, error);
        }
    },

    // adminNotificationList: async (req, res) => {
    //     try {
    //         let search = {
    //             notiTo: 'admin'
    //         }

    //         if (!req.body.startDate == '' && !req.body.endDate == "") {
    //             let endDate = new Date(req.body.endDate)
    //             endDate.setDate(endDate.getDate() + 1)
    //             console.log("endDates==>", endDate)
    //             search.createdAt = {
    //                 $gte: new Date(req.body.startDate),
    //                 $lte: endDate
    //             }
    //         }

    //         let result = await NOTIFICATONS.find(search).sort({createdAt : -1});
    //         if (result.length == 0) {
    //             return response.responseHandlerWithMessage(res, statusCode.DATAMISSING, "Notification data not found");
    //         } else {
    //             return response.responseHandlerWithData(res, statusCode.SUCCESS, "Notification data fetched successfully", result);
    //         }

    //     } catch (error) {
    //         console.log(`error is ${error}`)
    //         return response.responseHandlerWithMessage(res, 500, error);
    //     }

    // },

}