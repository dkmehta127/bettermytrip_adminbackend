const STATIC_CONTENT = require("../../models/staticModel");
const response = require("../../utility/httpResponseMessage");

module.exports = {

  //ABOUT US
  addAboutUs: async (req, res) => {
    try {
      const data = new STATIC_CONTENT({
        title: 'about_us',
        data: 'dummy data',
        is_for: 'app'
      });
      let dataAdd = await data.save();
      if (dataAdd) {
        return response.responseHandlerWithData(res, 200, `About us added successfully`, dataAdd);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  getAboutUs: async (req, res) => {
    try {
      let query = {
        is_for: 'app',
        title: 'about_us'
      }
      let aboutUsget = await STATIC_CONTENT.find(query, { data: 1, title:1 });
      if (aboutUsget) {
        return response.responseHandlerWithData(res, 200, `Data found`, aboutUsget[0]);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  updateAboutUs: async (req, res) => {
    try {
      let query = {
        _id: req.body._id,
        is_for: 'app',
        title: 'about_us'
      }

      let new_values = JSON.parse(JSON.stringify({
        data: req.body.data || undefined
      }))
      let catUpdate = await STATIC_CONTENT.findOneAndUpdate(
        query, new_values
      );
      if (catUpdate) {
        return response.responseHandlerWithData(res, 200, `About us updated`, catUpdate);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  //CONTACT US
  addContactUs: async (req, res) => {
    try {
      const data = new STATIC_CONTENT({
        title: 'contact_us',
        data: 'dummy data',
        contactNo: '9088998899',
        email: 'abc@bayin.com',
        address: 'UAE',
        is_for: 'app'
      });
      let dataAdd = await data.save();
      if (dataAdd) {
        return response.responseHandlerWithData(res, 200, `Contact Us added successfully`, dataAdd);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  getContactUs: async (req, res) => {
    try {
      let query = {
        is_for: 'app',
        title: 'contact_us'
      }
      let aboutUsget = await STATIC_CONTENT.find(query, { data: 1, title:1, contactNo:1, email:1, address:1 });
      if (aboutUsget) {
        return response.responseHandlerWithData(res, 200, `Data found`, aboutUsget[0]);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  updateContactUs: async (req, res) => {
    try {
      let query = {
        _id: req.body._id,
        is_for: 'app',
        title: 'contact_us'
      }

      let new_values = JSON.parse(JSON.stringify({
        data: req.body.data || undefined,
        contactNo: req.body.contactNo || undefined,
        email: req.body.email || undefined,
        address: req.body.address || undefined
      }))
      let catUpdate = await STATIC_CONTENT.findOneAndUpdate(
        query, new_values
      );
      if (catUpdate) {
        return response.responseHandlerWithData(res, 200, `Contact Us updated`, catUpdate);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  //T&C
  addTermsAndCond: async (req, res) => {
    try {
      const data = new STATIC_CONTENT({
        title: 'T&C',
        data: 'dummy data',
        is_for: 'app'
      });
      let dataAdd = await data.save();
      if (dataAdd) {
        return response.responseHandlerWithData(res, 200, `T&C added successfully`, dataAdd);
      }
    } catch (error) {
      console.log(error)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  getTermsAndCond: async (req, res) => {
    try {
      let query = {
        is_for: 'app',
        title: 'T&C'
      }
      let aboutUsget = await STATIC_CONTENT.find(query, { data: 1, title:1 });
      if (aboutUsget) {
        return response.responseHandlerWithData(res, 200, `Data found`, aboutUsget[0]);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  updateTermsAndCond: async (req, res) => {
    try {
      let query = {
        _id: req.body._id,
        is_for: 'app',
        title: 'T&C'
      }

      let new_values = JSON.parse(JSON.stringify({
        data: req.body.data || undefined
      }))
      let catUpdate = await STATIC_CONTENT.findOneAndUpdate(
        query, new_values
      );
      if (catUpdate) {
        return response.responseHandlerWithData(res, 200, `T&C updated`, catUpdate);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  //PRIVACY & POLICY
  addPrivacyAndPolicy: async (req, res) => {
    try {
      const data = new STATIC_CONTENT({
        title: 'P&P',
        data: 'dummy data',
        is_for: 'app'
      });
      let dataAdd = await data.save();
      if (dataAdd) {
        return response.responseHandlerWithData(res, 200, `P&P added successfully`, dataAdd);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  getPrivacyAndPolicy: async (req, res) => {
    try {
      let query = {
        is_for: 'app',
        title: 'P&P'
      }
      let aboutUsget = await STATIC_CONTENT.find(query, { data: 1,title:1 });
      if (aboutUsget) {
        return response.responseHandlerWithData(res, 200, `Data found`, aboutUsget[0]);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },

  updatePrivacyAndPolicy: async (req, res) => {
    try {
      let query = {
        _id: req.body._id,
        is_for: 'app',
        title: 'P&P'
      }

      let new_values = JSON.parse(JSON.stringify({
        data: req.body.data || undefined
      }))
      let catUpdate = await STATIC_CONTENT.findOneAndUpdate(
        query, new_values
      );
      if (catUpdate) {
        return response.responseHandlerWithData(res, 200, `P&P  updated`, catUpdate);
      }
    } catch (error) {
      console.log(`error is ${error}`)
      return response.responseHandlerWithMessage(res, 500, error);
    }
  },
  
}