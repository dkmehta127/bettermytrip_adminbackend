const NOTIFICATONS = require('../../models/notificationModel')



const {
    ObjectId
} = require('mongodb');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const response = require("../../utility/httpResponseMessage");
const statusCode = require("../../utility/httpResponseCode");
const Image = require('../../utility/utilityMethods');
const sendMail = require("../../utility/sendMail");
const PUSH = require('../../utility/pushNotification')

const {
    query,
    body,
    check,
    oneOf,
    validationResult
} = require('express-validator');
const errorFormatter = ({
    location,
    msg,
    param,
    value,
    nestedErrors
}) => {
    return `${location}[${param}]: ${msg}`;
};

const date = require('date-and-time');
const moment = require('moment');
var ISODate = require("isodate");


module.exports = {

    addNotifications: async (req, res) => {
        try {
            const newNotifications = new NOTIFICATONS({
                title: req.body.title,
                content: req.body.content,
                for: 'admin'
            })

            let saveNotifications = await newNotifications.save()
            if (saveNotifications) {
                //Send Notifications
                // PUSH.adminSendNotification({
                //     title: req.body.title,
                //     content: req.body.content
                // })

                return response.responseHandlerWithData(res, statusCode.SUCCESS, "Notifications saved sucessfully", saveNotifications);
            }
        } catch (error) {
            console.log(error)
            return response.responseHandlerWithMessage(res, 500, error);
        }
    },

    notificationsList: async (req, res) => {
        try {

            let query = {
                for: 'admin'
            }
            if (req.body.startDate && req.body.endDate) {
                let endDate = new Date(req.body.endDate)
                endDate.setDate(endDate.getDate() + 1)
                console.log("endDates==>", endDate)
                query.createdAt = {
                    $gte: new Date(req.body.startDate),
                    $lte: endDate
                }
            }

            if (req.body.search) {
                query.$or = [{
                        "title": {
                            $regex: req.body.search,
                            $options: 'i'
                        }
                    },
                    {
                        "content": {
                            $regex: req.body.search,
                            $options: 'i'
                        }
                    }
                ]
            }

            let result = await NOTIFICATONS.find(query);
            if (result.length == 0) {
                return response.responseHandlerWithMessage(res, statusCode.DATAMISSING, "Data Not Found");
            } else {
                return response.responseHandlerWithData(res, statusCode.SUCCESS, "Data fetched successfully", result);
            }

        } catch (error) {
            console.log(error)
            return response.responseHandlerWithMessage(res, 500, error);
        }
    },

    resendNotifications: async (req, res) => {
        try {
            let updateNotification = await NOTIFICATONS.findOneAndUpdate({
                _id: req.body._id,
                for: 'admin'
            }, { 
                title: req.body.title,
                content: req.body.content,
            })
            if (updateNotification) {
                //Send Notifications
                // PUSH.adminSendNotification({
                //     title: req.body.title,
                //     content: req.body.content
                // })
                return response.responseHandlerWithData(res, statusCode.SUCCESS, "Notifications resend sucessfully", updateNotification);
            }
        } catch (error) {
            console.log(error)
            return response.responseHandlerWithMessage(res, 500, error);
        }
    },

}