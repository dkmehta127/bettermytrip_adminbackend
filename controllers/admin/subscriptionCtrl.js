const SUBSCRIPTION = require('../../models/subscriptionModel')



const {
    ObjectId
} = require('mongodb');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const response = require("../../utility/httpResponseMessage");
const statusCode = require("../../utility/httpResponseCode");
const Image = require('../../utility/utilityMethods');
const sendMail = require("../../utility/sendMail");


const {
    query,
    body,
    check,
    oneOf,
    validationResult
} = require('express-validator');
const errorFormatter = ({
    location,
    msg,
    param,
    value,
    nestedErrors
}) => {
    return `${location}[${param}]: ${msg}`;
};

const date = require('date-and-time');
const moment = require('moment');
var ISODate = require("isodate");


module.exports = {

    addsubscription: async (req, res) => {
        try {
            await body('title').not().isEmpty().run(req);
            await body('duration').not().isEmpty().run(req);

            await body('price').not().isEmpty().run(req);
            await body('description').not().isEmpty().run(req);

            const errors = validationResult(req).formatWith(errorFormatter);;
            if (!errors.isEmpty()) {
                return response.responseHandlerWithData(res, statusCode.DATAMISSING, "Please check your request", errors.array());
            }

            


            
            const savedData = new SUBSCRIPTION({
                status:req.body.status,
                title: req.body.title,
                duration:req.body.duration,

                price: req.body.price,
                description: req.body.description,

            })

            let insertedData = await savedData.save();
            if(insertedData){
             
                return response.responseHandlerWithMessage(res, statusCode.SUCCESS, "Subscription added successfully", insertedData);
            }
        } catch (error) {
            response.log("Error is============>", error)
            return response.responseHandlerWithMessage(res, statusCode.ERROR, "Internal server error");
        }
    },

    subscriptionList: async (req, res) => {
        try {
            var search = {
                is_delete: false,
                       }
            var dateQuery = {
                is_delete: false,
            }

            let page_val = req.body.page || 1
            let skip = (page_val - 1) * 10
            let limit = 10
            let sort = {
                createdAt: -1
            }

            if (req.body.search) {
                search.$or = [{
                        "title": {
                            $regex: req.body.search,
                            $options: 'i'
                        }
                    },
                    // {
                    //     "institute_number": {
                    //         $regex: req.body.search,
                    //         $options: 'i'
                    //     }
                    // },
                    // {
                    //     "officialMobileNumber": {
                    //         $regex: req.body.search,
                    //         $options: 'i'
                    //     }
                    // },
                ]
            }
            if (req.body.status) {
                search.status = req.body.status
            }
            
            
            if (!req.body.startDate == '' && !req.body.endDate == "") {
                let endDate = new Date(req.body.endDate)
                endDate.setDate(endDate.getDate() + 1)
                console.log("endDates==>", endDate)
                dateQuery.createdAt = {
                    $gte: new Date(req.body.startDate),
                    $lte: endDate
                }
            }
            if (req.body.timeFrame === "Today") {
                //    let startTodayDate = new Date(moment().format())
                let setTodayDate = new Date()
                setTodayDate.setHours(0, 0, 59, 999);
                let todayDate = new Date()
                todayDate.setHours(23, 59, 59, 999);
                dateQuery.createdAt = {
                    $gte: setTodayDate,
                    $lte: todayDate
                }
            }
            if (req.body.timeFrame == "Week") {
                let weekDate = new Date(moment().startOf('isoWeek').format())
                let todayDate = new Date()
                todayDate.setHours(23, 59, 59, 999);
                dateQuery.createdAt = {
                    $gte: weekDate,
                    $lte: todayDate
                }
            }
            if (req.body.timeFrame === "Month") {
                let monthDate = new Date(moment().clone().startOf('month').format())
                let todayDate = new Date()
                todayDate.setHours(23, 59, 59, 999);
                dateQuery.createdAt = {
                    $gte: monthDate,
                    $lte: todayDate
                }
            }

            let result = await SUBSCRIPTION.aggregate([

                {
                    $match: search,
                },
                {
                    $match: dateQuery,
                },
                {
                    $sort: sort
                },
                {
                    $skip: skip
                },
                {
                    $limit: limit
                }

            ])
            result = await SUBSCRIPTION.aggregate([
                {
                    $sort: sort
                },
                {
                    $skip: skip
                },
                {
                    $limit: limit
                }

         

])
            let total =  await SUBSCRIPTION.aggregate([

                {
                    $match: search,
                },
                {
                    $match: dateQuery,
                },
            ])
            if (result.length == 0) {
                let data = {
                    total : total.length,
                    result : [],
                    page : req.body.page
                }
                return response.responseHandlerWithData(res, statusCode.SUCCESS, "Data not found", data);
            } else {
                let data = {
                    total : total.length,
                    result : result,
                    page : req.body.page
                }
                return response.responseHandlerWithData(res, statusCode.SUCCESS, "Data fetched successfully", data);
            }


        } catch (error) {
            console.log(`error is ${error}`)
            return response.responseHandlerWithMessage(res, 500, error);
        }

    },

    updateSubcription: async (req, res) => {
        try {
            let query = {
                _id: req.body._id
            }
            let subscriptionUpdatableData = {}
            let check = await SUBSCRIPTION.findOne(query)
            if (check) {
                
                if (req.body.title) {
                    let subscriptionTitle = await SUBSCRIPTION.findOne({
                        title: req.body.title,
                        _id: {
                            $ne: req.body._id
                        }
                    })
                    if (subscriptionTitle) {
                        return response.responseHandlerWithMessage(res, statusCode.ERROR, "subscription already exist with this title name");
                    } else {
                        subscriptionUpdatableData.title = req.body.title
                    }
                }
                if (req.body.description) {
                    subscriptionUpdatableData.description = req.body.description
                }
                if (req.body.duration) {
                    subscriptionUpdatableData.duration = req.body.duration
                }
                if (req.body.price) {
                    subscriptionUpdatableData.price = req.body.price
                }

                let subscription_update = await SUBSCRIPTION.findOneAndUpdate(query, subscriptionUpdatableData);
                if (subscription_update) {
                    return response.responseHandlerWithMessage(res, 200, "Data updated sucessfully");
                }
            } else {
                return response.responseHandlerWithMessage(res, 500, "Data not found");
            }
        } catch (error) {
            console.log(`error is ${error}`)
            return response.responseHandlerWithMessage(res, 500, error);
        }
    },

    removesubscription: async (req, res) => {
        try {
            await body('_id').not().isEmpty().run(req);
            const errors = validationResult(req).formatWith(errorFormatter);;
            if (!errors.isEmpty()) {
                return response.responseHandlerWithData(res, statusCode.DATAMISSING, "Please check your request", errors.array());
            }

            let removeDetails = await SUBSCRIPTION.findOneAndUpdate({
                _id: ObjectId(req.body._id)
            }, {
                $set: {
                    is_delete: true,
                    status : 'Inactive'
                }
            })

            return response.responseHandlerWithMessage(res, 200, 'Deleted successfully');
        } catch (error) {
            console.log(`error is ${error}`)
            return response.responseHandlerWithMessage(res, 500, error);
        }

    },
   
}
