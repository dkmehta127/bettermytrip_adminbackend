const TWILIO_ACCOUNT_SID = "AC907c14b3b7c662485713e703d1b255e6"
const TWILIO_AUTH_TOKEN = "e7f82912a454cc4415bbcc1863c7f34f"
const TWILIO_SENDER_PHONE = '+18145592537'

module.exports = {
    send: async (phone, message) => {
        if (
            phone &&
            message
        ) {
            try {
                const client = require('twilio')(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

                let SMSResponse = client.messages
                    .create({
                        body: message,
                        messagingServiceSid: 'MGae30fd3c1b90ff7f5f67187d7d0b5101',
                        to: phone
                    })
                if (SMSResponse) {
                    console.log('SMS Success: ', SMSResponse.to, SMSResponse.body)
                    return true
                }
            } catch (error) {
                console.log('SMS Error: ', phone, error.message, error)
                return false
            }
        } else {
            return false
        }
    }
}


// // const client = require('twilio')(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

// client.messages
//     .create({
//         body: 'hiiii sir this is test message',
//         messagingServiceSid: 'MGae30fd3c1b90ff7f5f67187d7d0b5101',
//         to: '+17035898296'
//     })
//     .then(message => console.log(message))
//     .done();