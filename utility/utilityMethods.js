const ExcelJS = require('exceljs');
let xlsx = require('node-xlsx');
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const fs = require('fs');

module.exports = {
    getJwtToken : async (data) => {
        let token = await jwt.sign(
            {
                data: data,
                exp: Date.now() + 60 * 60 * 1000,
            },
            'sUpER@SecReT');
        return token
    },

    encryptPassword : async (data) => {
        let salt = await bcrypt.genSalt(10)
        if (salt) {
            let hash = await bcrypt.hash(data, salt)
            if (hash) {
                return hash
            }
            else
                return err
        }
        else
            return err
    },

    zipRead: async (file, path) => {

        return new Promise((resolve, reject) => {
            var tmp_paths = file;
            var obj = xlsx.parse(fs.readFileSync(tmp_paths));
            // parses a buffer
            // console.log("objLength==>",obj[0].data)
            resolve(obj[0].data);

            //     var obj = xlsx.parse(fs.readFileSync(tmp_paths)); // parses a buffer
            //     console.log("objLength==>", obj[0].data.length)
            //     resolve(obj[0].data);
            // })

            // excelParser.worksheets({
            //     inFile: tmp_paths
            // }, function (err, worksheets) {
            //     if (err) console.error(err);
            //     consol.log(worksheets);
            // });

            return
        })

    }
}