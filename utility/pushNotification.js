const NOTIFICATONS = require("../models/notificationModel");
const USER = require('../models/adminModel')


const {
    ObjectId
} = require('mongodb');

const response = require("../utility/httpResponseMessage");

var admin = require("firebase-admin");
var serviceAccount = require("../config/firebase-config.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

module.exports = {

    appNotification: async (req, res) => {
        try {
            const message = {
                notification: {
                    title: req.title,
                    body: req.content,
                    // data : req.data
                },
                data: {
                    title: req.title,
                    body: req.content,
                    data: req.data
                },
                android: {
                    notification: {
                        icon: 'stock_ticker_update',
                        color: '#7e55c3'
                    }
                },
                token: req.token
            };
            admin.messaging().send(message)
                .then(async (response) => {
                    const newNotifications = new NOTIFICATONS({
                        notiTo: req.user,
                        notiMessage: req.content,
                        notiTitle: req.title,
                        data: req.data,
                        type:req.type,
                    })
                    let saveNotifications = await newNotifications.save()
                    console.log('Successfully sent message:', response);
                })
                .catch((error) => {
                    console.log('Error sending message:', error);
                })
        } catch (error) {
            console.log(error)
            return response.responseHandlerWithMessage(res, 500, error);
        }
    },

    // kitchenNotification: async (req, res) => {
    //     try {
    //         let getRegistrationToken = await STAFF.find({
    //             stote: ObjectId(req.store),
    //         });
    //         console.log('registrationToken--------------------------------------------------------------->', getRegistrationToken.length)
    //         if (getRegistrationToken && getRegistrationToken.length > 0) {
    //             for (let i = 0; i < getRegistrationToken.length; i++) {
    //                 const message = {
    //                     notification: {
    //                         title: req.title,
    //                         body: req.content,
    //                         // data : req.data
    //                     },
    //                     data: {
    //                         title: req.title,
    //                         body: req.content,
    //                         data: req.data
    //                     },
    //                     android: {
    //                         notification: {
    //                             icon: 'stock_ticker_update',
    //                             color: '#7e55c3'
    //                         }
    //                     },
    //                     token: getRegistrationToken[i].deviceToken
    //                 };
    //                 admin.messaging().send(message)
    //                     .then(async (response) => {
    //                         const newNotifications = new NOTIFICATONS({
    //                             notiTo: getRegistrationToken[i]._id.toString(),
    //                             notiMessage: req.content,
    //                             notiTitle: req.title,
    //                             data: req.data,
    //                             type:req.type,
    //                         })
    //                         let saveNotifications = await newNotifications.save()
    //                         console.log('Successfully sent message:', response);
    //                     })
    //                     .catch((error) => {
    //                         console.log('Error sending message:', error);
    //                     })
    //             }
    //         }
    //     } catch (error) {
    //         console.log(error)
    //         return response.responseHandlerWithMessage(res, 500, error);
    //     }
    // },

    adminSendNotification: async (req, res) => {
        try {
            let getRegistrationToken = await USER.find({
                deviceToken: {
                    $ne: ''
                },
                status: "Active",
            });
            console.log('registrationToken--------------------------------------------------------------->', getRegistrationToken.length)
            if (getRegistrationToken && getRegistrationToken.length > 0) {
                for (let i = 0; i < getRegistrationToken.length; i++) {
                    const message = {
                        notification: {
                            title: req.notiTitle,
                            body: req.notiMessage
                        },
                        data: {
                            title: req.notiTitle,
                            body: req.notiMessage,
                            // data: req.data
                        },
                        android: {
                            notification: {
                                icon: 'stock_ticker_update',
                                color: '#7e55c3'
                            }
                        },
                        token: getRegistrationToken[i].deviceToken
                    };
                    admin.messaging().send(message)
                        .then(async (response) => {
                            const newNotifications = new NOTIFICATONS({
                                user: getRegistrationToken[i]._id,
                                notiTitle: req.notiTitle,
                                notiMessage: req.notiMessage,
                                for: 'user'
                            })
                            let saveNotifications = await newNotifications.save()
                            console.log('Successfully sent message:', response);
                        })
                        .catch((error) => {
                            console.log('Error sending message:', error);
                        })
                }
            }
        } catch (error) {
            console.log(error)
            return response.responseHandlerWithMessage(res, 500, error);
        }
    },

}


// const registrationToken = "f0v_RXWxxWU1ANtUbIJ7OD:APA91bGx5sK8vai3HPB7KksC0VQ83PhRWEsL_snBiDpH5OvbqJvbQl56P5DYXzagL74hpZDOr92SnM8baU7MYOH-OoasnCiMdz9wrkYYDMoDbIR07ecswJP8eNDRmuBhkbiriabhTWTY"
// const message = {
//     notification: {
//         title: 'Test Title123',
//         body: 'Test Body'
//     },
//     android: {
//         notification: {
//             icon: 'stock_ticker_update',
//             color: '#7e55c3'
//         }
//     },
//     token: registrationToken
// };
// admin.messaging().send(message)
//     .then((response) => {
//         console.log('Successfully sent message:', response);
//     })
//     .catch((error) => {
//         console.log('Error sending message:', error);
// })
